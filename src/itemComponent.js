import React from 'react';
import Button from '@atlaskit/button';
import './../node_modules/@atlaskit/css-reset/dist/bundle.css';

export default class ItemComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  getButton() {
    if ( this.props.quantity > 0 ) {
      return (
        <div>
          <Button onClick={() => {
            this.props.onRemoveItem(this.props.id);
          } }>-</Button>
          {this.props.quantity}
          <Button onClick={() => {
            this.props.onAddItem(this.props.id);
          } }>+</Button>
        </div>);
    } else {
      return <button onClick = { () => {
        this.props.onAddItem(this.props.id);
      } } > Add </button>;
    }
  }
  render() {
    return (
      <div className={'card itemCard'}>
        <div>
          <img className={'card-top'} src={this.props.image}/>
          <div className={'card-body'}>
            <div className={'card-text'}>
              <p> {this.props.brandName} </p>
              <h6> {this.props.name}</h6>
              <h6> {this.props.details}</h6>
              <p className="price"><b>₹{this.props.price}</b></p>
              {this.getButton()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// @flow

import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './common.css';
import CartItemComponent from './CartItemComponent';

type Props = {
  data: Object,
  onCartItemAdded: number => void,
  onCartItemRemoved: number => void
}

export default class CartComponent extends React.Component<Props> {
  render() {
    if (this.props.data == null
      || this.props.data['count'] === null
      || this.props.data['count'] === undefined
      || this.props.data['count'] === -1) {
      return (
        <div className={'card'}>
          <img src={'https://www.pymnts.com/wp-content/uploads/2016/09/shoppingcartwalmart.jpg'}
            alt={'Shopping cart'}
            width={'100%'}
            height={'100%'}
          />
        </div>
      );
    }
    console.log('props are', this.props.data);
    const count = this.props.data['count'];

    let totalPrice = 0;
    const list = [];

    let added = 0;
    for (let id = 0; id <= count; ++id) {
      //ignore items with quantity === 0
      if (this.props.data[id]['quantity'] === 0) continue;
      ++added;
      //add to total price
      totalPrice += this.props.data[id]['price']
        * this.props.data[id]['quantity'];

      list.push(
          <div key={id} className={'row'} style={{paddingBottom: '2%'}}>
            <span className={'col-xs-12 col-sm-12 col-md-7 col-lg-4'}>{
              list.length + 1}.&nbsp; {this.props.data[id]['name']}
            </span>
            <span className={'col-xs-12 col-sm-12 col-md-5 col-lg-8'}>
              <CartItemComponent
                id={id}
                uprice={this.props.data[id]['price']}
                quantity={this.props.data[id]['quantity']}
                onItemAdded={this.props.onCartItemAdded}
                onItemRemoved={this.props.onCartItemRemoved}
              />
            </span>
          </div>
      );
    }
    if (added === 0) {
      return (
        //text-center only for cart image
        <div className={'card text-center'}>
          <h2 className={'text-left'}>Your cart summary</h2>
          <img src={require('./empty-cart.png')}
            alt={'Shopping cart'}
            width={'80%'}
            height={'80%'}
          />
        </div>
      );
    }
    return (
      <div className={'card cartCard'} >
        <h2>Your cart summary</h2>
        <h4 style={{margin: '3%'}}>
          {list.length} item(s) added
        </h4>
        <div className={'container-fluid'}>
          {list}
        </div>
        <div>
          <b className={'float-left'}>
            Subtotal:
          </b>
          <b className={'float-right'}>
            {totalPrice}
          </b>
        </div>
      </div>
    );
  }
}

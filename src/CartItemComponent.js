// @flow

import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './common.css';
import Button from '@atlaskit/button';

type Props = {
  id: number,
  uprice: number,
  quantity: number,
  onItemAdded: number => void,
  onItemRemoved: number => void,
}

export default class CartItemComponent extends React.Component<Props> {
  render() {
    return (
      <div className={'container-fluid'}>
        <div className={'row'}>
          <span className={'col-md-6 col-lg-7'}>
            <Button
              onClick={() => this.props.onItemRemoved(this.props.id)}
            >-
            </Button>
            <span
              style={{margin: '2%'}}
             >
              {this.props.quantity}
            </span>
            <Button
              onClick={() => this.props.onItemAdded(this.props.id)}
            >+
            </Button>
          </span>
          <span className={'col-md-6 col-lg-5'}>
          ₹{this.props.uprice * this.props.quantity}
          </span>
        </div>
      </div>
    );
  }
}

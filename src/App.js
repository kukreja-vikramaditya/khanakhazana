// @flow
// More on flow plugins here https://github.com/gajus/eslint-plugin-flowtype#installation

import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './../node_modules/@atlaskit/css-reset/dist/bundle.css';
import CartComponent from './CartComponent';
import Button from '@atlaskit/button';
import ItemComponent from './itemComponent';

/**
 * Data in localStorage is stored as a string by converting the information in
 * state.
 *
 * Item has the following fields
 *  - id
 *  - image
 *  - brandName
 *  - name
 *  - details
 *  - price
 *  - quantity
 */
export default class App extends React.Component {

  async componentWillMount() {
    // this function should be extracted out
    const totalCount = localStorage.getItem('count');
    if (totalCount === null || totalCount < 0) {
      localStorage.clear();

      // need to download fresh data
      const response = await fetch('https://api.myjson.com/bins/suqut');
      const data = await response.json();

      //data is an array of items
      data.forEach((item) => {
        //append quantity information
        item['quantity'] = 0;
        //api is 1 based indexing, make it 0 based
        --item['id'];

        //save item in localStorage at item's id
        localStorage.setItem(item['id'], JSON.stringify(item));
        //0 based counting TODO fix me maybe?
        localStorage.setItem('count', item['id']);
        //also update state with count and items
        this.setState({
          count: item['id'],
          [item['id']]: item,
        });
      });
    }

    for (let i = 0; i <= totalCount; ++i) {
      this.setState({
        count: i,
        [i]: JSON.parse(localStorage.getItem(i)),
      });
    }
  }

  async clearCache() {
    //TODO doesn't automatically trigger a new download of data
    localStorage.clear();
    if (this.state === null || this.state.count === null) return;
    await this.setState({count: -1});
    localStorage.setItem('count', -1);
    console.log('state is', this.state);
  };

  removeItem = (id) => {
    const copyId = JSON.parse(localStorage.getItem(id));
    --copyId['quantity'];
    localStorage.setItem( id, JSON.stringify(copyId));
    this.setState({[id]: copyId});
  };

  addItem = (id) => {
    const copyId = JSON.parse(localStorage.getItem(id));
    ++copyId['quantity'];
    localStorage.setItem( id, JSON.stringify(copyId));
    this.setState({[id]: copyId});
  };

  getItems() {
    const list = [];
    for (let i=0; i < this.state.count; i++) {
      list.push(
          <div className={'col-lg-3'}>
            <div className={'item'}>
              <ItemComponent
                id={this.state[i].id}
                quantity={this.state[i].quantity}
                image={this.state[i].image}
                brandName={this.state[i].brandName}
                name={this.state[i].name}
                details={this.state[i].details}
                price={this.state[i].price}
                onAddItem={this.addItem}
                onRemoveItem={this.removeItem}
              />
            </div>
          </div>
      );
    }
    return list;
  }

  getCart() {
    return (
      <div className={'col-xs-12 col-sm-12 col-md-12 col-lg-4' } style={{ position: 'relative'}}>
        <CartComponent
          data={this.state}
          onCartItemAdded={this.addItem}
          onCartItemRemoved={this.removeItem}
        />
      </div>
    );
  }

  render() {
    return (
      <div className={'container-fluid appContainer'}>
        Masala & Spices ({this.state.count+1} items)
        <div className={'row my-row'}>
          <div className={'col-xs-12 col-sm-12 col-md-12 col-lg-8'}>
            {this.getItems()}
          </div>
          {this.getCart()}
        </div>
      </div>

    );
  }
}
